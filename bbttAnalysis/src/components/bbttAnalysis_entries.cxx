#include "../BaselineVarsbbttAlg.h"
#include "../MMCDecoratorAlg.h"
#include "../MMCSelectorAlg.h"
#include "../HHbbttSelectorAlg.h"

using namespace HHBBTT;

DECLARE_COMPONENT(BaselineVarsbbttAlg)
DECLARE_COMPONENT(MMCDecoratorAlg)
DECLARE_COMPONENT(MMCSelectorAlg)
DECLARE_COMPONENT(HHbbttSelectorAlg)
